# Introduction
This script takes a list of photos (in a certain order), sets their EXIF capture date to a pre-defined date and "spreads" the capture time over the day between the times specified.

Useful when you have received a bunch of photos through some messaging service that strips EXIF data.
If you know the date on which the photos were taken and can approximate the order of them, 
you can let this script come up with a fitting capture date and time.

# Usage

1. Create a filelist per day that contains on each line a path to a photo taken that day.
    * Filename should be of the following format: `YYYY-MM-DD_HHmm-HHmm.txt`. The first `HHmm` sets the (approximate) time at which the first photo was taken, the second `HHmm` sets the (approximate) endtime the photo was taken
    * Each line in the file should contain an absolute path to the photo.
2. Make sure you have python > 3.4 and [`exiftool`][1] available 
3. Run the script `imagine_exif.py`
4. The EXIF information of the files will be modified in place


## Example
An input file `2020-04-20_0900-1500.txt` with the following lines:

```txt
/home/user/photos/image001.jpg
/home/user/photos/image003.jpg
/home/user/photos/image002.jpg
/home/user/photos/image004.jpg
```
will result in the four files having the following EXIF datetimes (in the `DateTimeOriginal` and the `CreateDate` tag):

* image001 - 2020-04-20T09:00:00
* image003 - 2020-04-20T11:00:00
* image002 - 2020-04-20T13:00:00
* image004 - 2020-04-20T15:00:00

[1]: https://exiftool.org/