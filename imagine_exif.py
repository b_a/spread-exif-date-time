"""
Comes up with a list of 'imagined' datetimes for a set of images based on manually
create filelists.
"""
from pathlib import Path as p
from subprocess import call
from datetime import datetime as dt
import sys

# Set root path
root_path = p.cwd()

# Get all file lists
input_files = list(root_path.glob('*.txt'))
print(f"Found {len(input_files)} input files:")
[print(str(x)) for x in input_files]
value = input("Process these? (y/n)")
if value == 'n':
    sys.exit("User asked not to continue")


def list_of_times(start, end, desired_length):
    """
    Return a list of times of between two times (inclusive)

    :param start: start time in "HHMM" format
    :param end: end time in "HHMM" format
    :param desired_length: length of the desired list
    :return: a list of strings
    """
    # Convert start and end to unixtime
    u_start = int(dt.strptime("1970 " + start, "%Y %H%M").timestamp())
    u_end = int(dt.strptime("1970 " + end, "%Y %H%M").timestamp())
    # Check if the end time is after the start time
    assert u_end > u_start, "End time is before or equal to start time."
    # Determine the desired step in seconds
    # (`desired_length - 1` because then the end time can be included later on)
    u_step = round((u_end - u_start) / (desired_length - 1))
    # Get a range of unixtimes and convert to datetimes
    unixtimes = list(range(u_start, u_end, u_step))
    datetimes = [dt.fromtimestamp(x) for x in unixtimes]
    # Convert to HHmm format again
    times = [x.strftime("%H%M") for x in datetimes]
    # Append the end time to include it in the list
    times.append(end)
    # Return
    return times


# Load file list
for input_file in input_files:
    # Load input file into list, every entry is an image
    raw_file = open(str(input_file), "r")
    file_array = raw_file.read().splitlines()
    raw_file.close()
    # Parse filename of input
    filename = input_file.stem
    print(f"Found {len(file_array)} images in {filename}")
    # Parse date and time from filename
    year, month, day = [filename[0:4], filename[5:7], filename[8:10]]
    start_time, end_time = [filename[11:15], filename[16:20]]
    # List of times
    exif_times = list_of_times(start_time, end_time, len(file_array))
    exif_date = f"{year}:{month}:{day}"
    for idx, file in enumerate(file_array):
        # Check if file exists
        if p(file).exists():
            # Set EXIF date and time
            exif_time = f"{str(exif_times[idx])[0:2]}:{str(exif_times[idx])[2:5]}:00"
            exif_date_time = exif_date + " " + exif_time
            # Set the DateTimeOriginal and CreateDate
            call(['exiftool',
                  '-overwrite_original',
                  f"-DateTimeOriginal={exif_date_time}",
                  f"-CreateDate={exif_date_time}",
                  file])
        else:
            print(f"{file} does not exist")
